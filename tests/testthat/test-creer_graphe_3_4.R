test_that("creer_graphe_3_4 fonctionne", {

  # Test que le graphe est un ggplot
  objet <- creer_graphe_3_4(millesime_stock_artif = 2020,millesime_population = 2018,code_reg = '52')
  testthat::expect_equal(attr(objet, "class"), c("gg","ggplot"))

})
