test_that("creer_graphe_1_5 fonctionne", {

  # Test que le graphe est un ggplot

  objet <- creer_graphe_1_5(code_reg = "52")
  testthat::expect_equal(attr(objet, "class"), c("gg","ggplot"))

})
